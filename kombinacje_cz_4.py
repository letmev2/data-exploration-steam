import pandas as pd

#UWAGA!!! W przypadku gdy ilosc_par>2, nalezy wyłączyć opcjonalny warunek na dole programu!!!!!!!!
ilosc_par=2      #ustawia czy ma szukac kombinacji gier parami, trójkami, itd.

df1=pd.read_csv('D:/projekty/projektkraken/data-exploration-steam/klucz_gier.csv',
                sep=",")

df2=pd.read_csv(('D:/projekty/projektkraken/data-exploration-steam/permutacje_dla_'+str(ilosc_par)+'.csv'),
                sep=",")

df2=df2.drop('Unnamed: 0',1)
df2=df2.drop_duplicates(['kombinacje'], keep='last')
df2=df2.drop('kombinacje',1)
df2=df2.drop('uzytkow',1)


#print(df1.head())
#print(df2.head())
for i in range(ilosc_par):
    df2.rename(columns={('gra'+str(i+1)):'id_gry'},inplace=True)
    df2=pd.merge(df2,df1,on='id_gry')
    df2=df2.drop('Unnamed: 0',1)
    df2.rename(columns={'nazwa':('gra'+str(i+1))},inplace=True)
    df2.rename(columns={'id_gry':('id_gry'+str(i+1))},inplace=True)

print(df2.head())

df3=pd.read_csv('D:/projekty/projektkraken/data-exploration-steam/purchased.csv',
                sep=",")

df3= df3.drop('indeks.1', 1)
print(df3.head())

for i in range(ilosc_par):
    df4=df3.groupby(['nazwa']).size().reset_index(name=('ile_osob_kupilo_'+str(i+1)))
    df4 = pd.DataFrame(df4)
    df4.rename(columns={'nazwa': ('gra'+str(i+1))}, inplace=True)
    df2=pd.merge(df2,df4, on='gra'+str(i+1))

#print(df2.head(6))

#zakladamy ze lista gier zawiera tylko gry ktore zostaly przez kogos zakupione (wynika to z czesci1), wiec mianownik nie bedzie =0
for i in range(ilosc_par):
    df2['wsp_pop_wzg_gry'+str(i+1)]=(df2['ilosc_powtorzen']/df2['ile_osob_kupilo_'+str(i+1)]*100)
    #wspolczynnik popularnosci danej kombinacji wzg np gry1, czyli ilu sposrod wszystkich nabywcow gry1 zdecydowalo sie na kupno gry2


#print(df2[((df2['wsp_pop_wzg_gry1']==df2['wsp_pop_wzg_gry2']) & (df2['wsp_pop_wzg_gry2']==100))])
#OPCJONALNY WARUNEK TYLKO DLA ILOSC_PAR=2
df2=df2[~((df2['wsp_pop_wzg_gry1']==df2['wsp_pop_wzg_gry2']) & (df2['wsp_pop_wzg_gry2']==100))]

#OPCJONALNY WARUNEK TYLKO DLA ILOSC_PAR=3
#df2=df2[~((df2['wsp_pop_wzg_gry1']==df2['wsp_pop_wzg_gry2']) & (df2['wsp_pop_wzg_gry2']==df2['wsp_pop_wzg_gry3']) & (df2['wsp_pop_wzg_gry2']==100))]

df2.to_csv('wyniki_kombi_'+str(ilosc_par)+'.csv')

print(df2.head(6))
