import pandas as pd

#import itertools
from ast import literal_eval
df1=pd.read_csv('D:/dane/letme/core_submittedquestionnaire.csv',
                sep=",")#,index_col='id')

#print(df1)


def analizator(lista1,tekst1,lista2,tekst2):
    print('----------------------------------------------')
    print('Combinations '+str(tekst1) +' x ' +  str(tekst2)+':')
    numer1 = -1
    for i in lista1:
        numer2=-1
        numer1=numer1+1
        for j in lista2:
            numer2 = numer2 + 1
            dfxyz=pd.merge(i,j,on='id')
            #listamoodmultiplayer.append((dfxyz['id'].count()))
            print('How many people chose '+str(tekst1)+'_'+str(numer1)+' and '+str(tekst2)+'_'+str(numer2)+':   ' + str(dfxyz['id'].count()))

def kolejnosclisty(list1):
    list1=literal_eval(list1)  #konwertuje string np [1,2] na liste
    list1.sort(reverse=False)
    return list1


df1['themes']=df1.apply(
        lambda row: (kolejnosclisty(row['themes'])),axis=1)

dfmood0=df1[df1['mood_id']==0]
dfmood1=df1[df1['mood_id']==1]
#df4=df1[df1['mood'].isnull()]
#df0=pd.DataFrame()
print('----------------------------------------------')
print('People counted:                                     '+str(df1['id'].count()))
print('----------------------------------------------')
print('How many people chose mood=0 (chilling):            '+str(dfmood0['id'].count()))
print('How many people chose mood=1 (challenge):           '+str(dfmood1['id'].count()))
#print(df1)
print('----------------------------------------------')
dfmultiplayer0=df1[df1['multiplayer_id']==0]
dfmultiplayer1=df1[df1['multiplayer_id']==1]
#dfmultiplayer2=df1[df1['multiplayer_id']==2]
print('How many people chose multiplayer=0 (Multiplayer):  '+str(dfmultiplayer0['id'].count()))
print('How many people chose multiplayer=1 (Singleplayer): '+str(dfmultiplayer1['id'].count()))
#print('How many games have multiplayer=2:   '+str(dfmultiplayer2.size))
print('----------------------------------------------')
dfmainstream0=df1[df1['mainstream_id']==0]
dfmainstream1=df1[df1['mainstream_id']==1]
dfmainstream2=df1[df1['mainstream_id']==2]
print('How many people chose mainstream=0 (mainstream):    '+str(dfmainstream0['id'].count()))
print('How many people chose mainstream=1 (indie):         '+str(dfmainstream1['id'].count()))
print('How many people chose mainstream=2 (surprise):      '+str(dfmainstream2['id'].count()))
print('----------------------------------------------')
dfrecently_released0=df1[df1['recently_released_id']==0]
dfrecently_released1=df1[df1['recently_released_id']==1]
dfrecently_released2=df1[df1['recently_released_id']==2]
print('How many people chose recently_released=0:          '+str(dfrecently_released0['id'].count()))
print('How many people chose recently_released=1:          '+str(dfrecently_released1['id'].count()))
print('How many people chose recently_released=2:          '+str(dfrecently_released2['id'].count()))
print('----------------------------------------------')
#print(df1[df1.genres.apply(lambda x: 'indie' in x)])       #wyswietla te elementy ze slowo indie znajduje sie w liscie w kolumnie
#print('----------------------------------------------')
dftheme0=df1[df1.themes.apply(lambda x: 0 in x)]
print('How many people chose theme 0 (fantasy):            '+str(dftheme0['id'].count()))
dftheme1=df1[df1.themes.apply(lambda x: 1 in x)]
print('How many people chose theme 1 (anime):              '+str(dftheme1['id'].count()))
dftheme2=df1[df1.themes.apply(lambda x: 2 in x)]
print('How many people chose theme 2 (sci-fi):             '+str(dftheme2['id'].count()))
dftheme3=df1[df1.themes.apply(lambda x: 3 in x)]
print('How many people chose theme 3 (history):            '+str(dftheme3['id'].count()))
dftheme4=df1[df1.themes.apply(lambda x: 4 in x)]
print('How many people chose theme 4 (horror):             '+str(dftheme4['id'].count()))
dftheme5=df1[df1.themes.apply(lambda x: 5 in x)]
print('How many people chose theme 5 (war):                '+str(dftheme5['id'].count()))
dftheme6=df1[df1.themes.apply(lambda x: 6 in x)]
print('How many people chose theme 6 (shooting):           '+str(dftheme6['id'].count()))
dftheme7=df1[df1.themes.apply(lambda x: 7 in x)]
print('How many people chose theme 7 (comedy):             '+str(dftheme7['id'].count()))
dftheme8=df1[df1.themes.apply(lambda x: 8 in x)]
print('How many people chose theme 8 (comic):              '+str(dftheme8['id'].count()))
dftheme9=df1[df1.themes.apply(lambda x: 9 in x)]
print('How many people chose theme 9 (classic):            '+str(dftheme9['id'].count()))
dftheme10=df1[df1.themes.apply(lambda x: 10 in x)]
print('How many people chose theme 10 (racing):            '+str(dftheme10['id'].count()))
dftheme11=df1[df1.themes.apply(lambda x: 11 in x)]
print('How many people chose theme 11 (abstract):          '+str(dftheme11['id'].count()))
dftheme12=df1[df1.themes.apply(lambda x: 12 in x)]
print('How many people chose theme 12 (sport):             '+str(dftheme12['id'].count()))
dftheme13=df1[df1.themes.apply(lambda x: 13 in x)]
print('How many people chose theme 13 (other):             '+str(dftheme13['id'].count()))
print('----------------------------------------------')
#############################################   lista wszystkich wystepujacych gatunkow
lista_gatunkow=[]
def lista_genres(lista2):
    lista2=literal_eval(lista2)
    for i in lista2:
        if i not in lista_gatunkow:
            lista_gatunkow.append(i)

df1.apply(lambda row: (lista_genres(row['genres'])),axis=1)
lista_gatunkow_podstawowa=lista_gatunkow
print('Lista gatunków genres:')
print(lista_gatunkow_podstawowa)
lista_gatunkow=[]
df1.apply(lambda row: (lista_genres(row['more_genres'])),axis=1)
print('Lista rozszerzona tagów more_genres:')
print(lista_gatunkow)
print('----------------------------------------------')
dfgenres = {name: pd.DataFrame() for name in lista_gatunkow_podstawowa}
for name, df in dfgenres.items():
    dfgenres[str(name)] = df1[df1.genres.apply(lambda x: name in x)]
    print('How many games have genre '+str(name)+':   ' + str(dfgenres[str(name)]['id'].count()))
print('----------------------------------------------')
dfmore_genres = {name: pd.DataFrame() for name in lista_gatunkow}
for name, df in dfmore_genres.items():
    dfmore_genres[str(name)] = df1[df1.more_genres.apply(lambda x: name in x)]
    print('How many games have other (more) genre - '+str(name)+':   ' + str(dfmore_genres[str(name)]['id'].count()))


print('----------------------------------------------')
print('Combinations mood x multiplayer:')
dfmood0_multiplayer0=pd.merge(dfmood0,dfmultiplayer0,on=['id'])
print('How many people chose chilling and multiplayer: '+str(dfmood0_multiplayer0['id'].count()))
dfmood0_multiplayer0=[]
dfmood0_multiplayer1=pd.merge(dfmood0,dfmultiplayer1,on=['id'])
print('How many people chose chilling and singleplayer: '+str(dfmood0_multiplayer1['id'].count()))
dfmood0_multiplayer1=[]
dfmood1_multiplayer0=pd.merge(dfmood1,dfmultiplayer0,on=['id'])
print('How many people chose challenge and multiplayer: '+str(dfmood1_multiplayer0['id'].count()))
dfmood1_multiplayer0=[]
dfmood1_multiplayer1=pd.merge(dfmood1,dfmultiplayer1,on=['id'])
print('How many people chose challenge and singleplayer: '+str(dfmood1_multiplayer1['id'].count()))
dfmood1_multiplayer1=[]


print('----------------------------------------------')
print('Combinations mood x mainstream:')
dfmood0_mainstream0=pd.merge(dfmood0,dfmainstream0,on=['id'])
print('How many people chose chilling and mainstream: '+str(dfmood0_mainstream0['id'].count()))
dfmood0_mainstream0=[]
dfmood0_mainstream1=pd.merge(dfmood0,dfmainstream1,on=['id'])
print('How many people chose chilling and indie: '+str(dfmood0_mainstream1['id'].count()))
dfmood0_mainstream1=[]
dfmood0_mainstream2=pd.merge(dfmood0,dfmainstream2,on=['id'])
print('How many people chose chilling and surprise: '+str(dfmood0_mainstream2['id'].count()))
dfmood0_mainstream2=[]
dfmood1_mainstream0=pd.merge(dfmood1,dfmainstream0,on=['id'])
print('How many people chose challenge and mainstream: '+str(dfmood1_mainstream0['id'].count()))
dfmood1_mmainstream0=[]
dfmood1_mainstream1=pd.merge(dfmood1,dfmainstream1,on=['id'])
print('How many people chose challenge and indie: '+str(dfmood1_mainstream1['id'].count()))
dfmood1_mainstream1=[]
dfmood1_mainstream2=pd.merge(dfmood1,dfmainstream2,on=['id'])
print('How many people chose challenge and surprise: '+str(dfmood1_mainstream2['id'].count()))
dfmood1_mainstream2=[]
#print('----------------------------------------------')
#for



#listmood=[dfmood0,dfmood1]
#listmultiplayer=[dfmultiplayer0,dfmultiplayer1]
#analizator(listmood,'mood',listmultiplayer,'multiplayer')


listmood=[dfmood0,dfmood1]
listmultiplayer=[dfmultiplayer0,dfmultiplayer1]
listmainstream=[dfmainstream0,dfmainstream1,dfmainstream2]
listrecently_released=[dfrecently_released0,dfrecently_released1,dfrecently_released2]
listtheme=[dftheme0,dftheme1,dftheme2,dftheme3,dftheme4,dftheme5,dftheme6,dftheme7,dftheme8,dftheme9,dftheme10,dftheme11,dftheme12,dftheme13]
listgenres=[dfgenres['action'],dfgenres['simulation'],dfgenres['adventure'],dfgenres['rpg'],dfgenres['other'],dfgenres['strategy']]
listmore_genres=[dfmore_genres['openworld'],dfmore_genres['fighter'],dfmore_genres['storydriven'],dfmore_genres['life'],dfmore_genres['pointclick'],dfmore_genres['arcade'],
                 dfmore_genres['games'],dfmore_genres['simulation'],dfmore_genres['moba'],dfmore_genres['shooter'],dfmore_genres['platformer'],dfmore_genres['rts'],dfmore_genres['exploration'],
                 dfmore_genres['abstract'],dfmore_genres['war'],dfmore_genres['customisation'],
                 dfmore_genres['sport'],dfmore_genres['team'],dfmore_genres['freeroaming'],dfmore_genres['survival'],
                 dfmore_genres['construction'],dfmore_genres['scifi'],dfmore_genres['sports'],dfmore_genres['tactical'],dfmore_genres['turnbased'],
                 dfmore_genres['puzzle'],dfmore_genres['management'],dfmore_genres['interactivefiction'],dfmore_genres['narrative']]


#mood
analizator(listmood,'mood',listrecently_released,'recently_released')
analizator(listmood,'mood',listtheme,'theme')
analizator(listmood,'mood',listgenres,'genre')
#analizator(listmood,'mood',listmore_genres,'more_genre')

#multiplayer
analizator(listmultiplayer,'multiplayer',listrecently_released,'recently_released')
analizator(listmultiplayer,'multiplayer',listtheme,'theme')
analizator(listmultiplayer,'multiplayer',listgenres,'genre')
#analizator(listmultiplayer,'multiplayer',listmore_genres,'more_genre')

#mainstream
analizator(listmainstream,'mainstream',listrecently_released,'recently_released')
analizator(listmainstream,'mainstream',listtheme,'theme')
analizator(listmainstream,'mainstream',listgenres,'genre')
#analizator(listmainstream,'mainstream',listmore_genres,'more_genre')







#for k in listamoodmultiplayer:
 #   print('How many people chose challenge and surprise:   '+str(k))




#def kombos(zmienna1,wartosci1,zmienna2,wartosci2):
 #   for i in range(int(wartosci1)):
  #      for j in range(int(wartosci2)):
   #         dejtafrejm=pd.merge((('df'+(zmienna1)+str(wartosci1))),(('df'+(zmienna2)+str(wartosci2))),on=['id'])
#kombos('mood',2,'multiplayer',2)






