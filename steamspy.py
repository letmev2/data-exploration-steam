import pandas as pd

df1=pd.read_csv('D:/projekty/projektkraken/data-exploration-steam/data/steamspy.csv',
                sep=",")

#df1['Owners after']= df1[df1['Owners after'][0]]
#print(df1[df1['Owners after'][0]])
df1['Owners after']=pd.DataFrame(df1['Owners after'].str.split(' ',1).tolist(),
                                   columns = ['flips','row'])
#for x in df1['Owners after']:
   # print(x)
def usuwaprzecinek(x):
    x=int(x.replace(',',''))
    return x

df1=df1[['#','Game','Owners after','Userscore (Metascore)']]

df1['Owners after']=df1.apply(
        lambda row: (usuwaprzecinek(row['Owners after'])),axis=1)
df1['Owners after']=df1['Owners after'].replace(',',' ')
df1=df1.sort_values(by=['Owners after'],ascending=False)
df1=df1.rename(columns={'Owners after':'owners'})

print(df1.head(80))


df1.to_csv('steamspy_przetworzony.csv')