import pandas as pd

ilosc_par=2      #ustawia czy ma szukac kombinacji gier parami, trójkami, itd.

#w przypadku innego pliku z danymi niz ten z kaggle(lub jego modyfikacji/poszerzeniu o nowe gry),
# tabela/plik klucz_gier.csv generowana w czesci 1, musi zostac wygenerowana na nowo
df1=pd.read_csv('D:/projekty/projektkraken/data-exploration-steam/klucz_gier.csv',
                sep=",")

df2=pd.read_csv(('D:/projekty/projektkraken/data-exploration-steam/kombinacje'+str(ilosc_par)+'.csv'),
                sep=",")
#df2=df2[df2['kombinacje']=='(0, 1038)']
print(df2.head())


########################################
#df2=df2[df2['uzytkow']<700000]

#def listToStringWithoutBrackets(list1):
   # zmienna_podst=str(list1).replace('(','').replace(')','').replace(',', '').split()
  #  return zmienna_podst

#print(df2.head())
#df2['gra1']=0
#df2['gra2']=0
#df2['gra1'] =df2.apply(
 #   lambda row: (int(listToStringWithoutBrackets(row['kombinacje'])[0])),axis=1)
#df2['gra2'] =df2.apply(
 #   lambda row: (int(listToStringWithoutBrackets(row['kombinacje'])[1])),axis=1)
#df2[['gra1','gra2']]=(listToStringWithoutBrackets(df2['kombinacje']))
#df4['gra2']=int(listToStringWithoutBrackets(df4['kombinacje'])[2])
#print(df2.head())



######################## DZIALA, liczy ilosc powtorzen

df2['ilosc_powtorzen']=0 #ilosc_powtorzen bedzie mowic, ile razy gracze którzy kupili grę1, również kupili grę2; jest to liczba takich graczy łącznie, bazując na danych z kaggle
df3=df2.groupby('kombinacje')['ilosc_powtorzen'].count()
df3=pd.DataFrame(df3)
df2=df2.drop('ilosc_powtorzen', 1)
df3['kombinacje']=df3.index
df4=pd.merge(df2,df3, on='kombinacje')
df4=df4.drop('Unnamed: 0',1)
print(df4.head())
########################



############################################ Rozdziela (gra1,gra2) na dwie zmienne
def RemoveBrackets(list1):
    zmienna_podst=str(list1).replace('(','').replace(')','').replace(',', '').split()
    return zmienna_podst

for i in range(ilosc_par):
    df4['gra'+str(i+1)]=0
    df4['gra'+str(i+1)] =df4.apply(
        lambda row: (int(RemoveBrackets(row['kombinacje'])[i])),axis=1)

##df2[['gra1','gra2']]=(listToStringWithoutBrackets(df2['kombinacje']))
##df4['gra2']=int(listToStringWithoutBrackets(df4['kombinacje'])[2])
print(df4.head())
#############################################

df4.to_csv('permutacje_dla_'+str(ilosc_par)+'.csv')






