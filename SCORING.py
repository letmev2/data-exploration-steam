import pandas as pd
import numpy as np
import math
df = pd.DataFrame({'tytul':['wiedzmin','skyrim','diablo','starcraft','sc'],
                   'id':[1,2,3,4,5],
                    'genres':[[0,3,0,0,2,4,0,0,0,1,3,1,2,3,1,1,1,0,0,0,0],
                             [1,4,0,5,2,4,3,0,0,1,1,1,2,3,1,1,1,2,0,2,0],
                              [2, 4, 2, 5, 2, 4, 3, 0, 3, 1, 1, 2, 2, 3, 1, 4, 1, 1, 0, 2, 0],
                              [1, 3, 0, 4, 2, 3, 2, 1, 1, 2, 1, 4, 2, 3, 1, 2, 2, 2, 1, 2, 1],
                              [1, 3, 0, 4, 2, 3, 2, 1, 1, 2, 1, 4, 2, 3, 1, 2, 2, 2, 1, 2, 1]],

                   'subgenres':[[0,0,1,5,0,0,0,0,0,1,0,0,0,0,2,3,4,2,3,1,3,3,5,3,0,1],
                                [1, 2, 1, 4, 3, 0, 0, 2, 0, 1, 0, 0, 0, 5, 2, 3, 6, 2, 3, 3, 3, 5, 5, 3, 0, 1],
                                [1,2,1,4,3,0,0,0,2,1,2,0,0,1,2,3,1,2,3,4,3,2,5,1,0,1],
                                [4, 1, 1, 3, 0, 1, 2, 3, 1, 0, 2, 5, 2, 2, 5, 6, 3, 5, 3, 3, 5, 5, 3, 0, 1, 1],
                                [4, 1, 1, 3, 0, 1, 2, 3, 1, 0, 2, 5, 2, 2, 5, 6, 3, 5, 3, 3, 5, 5, 3, 0, 1, 1]
                                ],
                   'multiplayer':[1,2,2,0,0],
                   'mainstream':[0,1,0,1,0],
                   'recently_released':[0.25,1,1.4,3.13,3.12],  #nowosc gry moze byc zmienną ciągłą, gdzie 1 = równy 1 rok od premiery,
                   'mood':[1,0,0,2,1]

                   })
#print(df.head())
#genres=([0,3,0,0,2,4,0,0,0,1,3,1,2,3,1,1,1,0,0,0,0])
#subgenres=([0,0,1,5,0,0,0,0,0,1,0,0,0,0,2,3,4,2,3,1,3,3,5,3,0,1])

user_genres=([0,1,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,1])
user_subgenres=([0,0,1,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,1,0,1,1,0,0,0,1])

def fragment_A_B(wektor_genres,wektor_subgenres):
    suma_genres = 0
    suma_themes = 0
    suma_subgenres=0
    for i in range(7):
        suma_genres=suma_genres+wektor_genres[i]
    for i in range(14):
        suma_themes = suma_themes+wektor_genres[i+7]
    for i in range(26):
        suma_subgenres = suma_subgenres+wektor_subgenres[i]
    return suma_genres, suma_themes, suma_subgenres

def wyznaczanie_stalej_genres_themes(zsumowane_genres,zsumowane_themes):
    if zsumowane_genres==0:
        stala_genres = 1
    elif zsumowane_themes==0:
        stala_genres = 1
    else:
        stala_genres = zsumowane_themes/zsumowane_genres * 1.3
    return stala_genres

def mnozenie_przez_stala(wektor_genrowy,stala):
    for i in range(7):
        wektor_genrowy[i]=(stala*float(wektor_genrowy[i]))
    return wektor_genrowy

def normalizacja(wektor_genres_przed,suma_laczna_normaliz_g_t):
    for i in range(len(wektor_genres_przed)):
        wektor_genres_przed[i]=wektor_genres_przed[i]/suma_laczna_normaliz_g_t
    return (wektor_genres_przed)

def czesc_AB(genres,subgenres):
    suma_genres, suma_themes, suma_subgenres = fragment_A_B(genres,subgenres)
    stala_genres = wyznaczanie_stalej_genres_themes(suma_genres,suma_themes)
    przemnozony_wektor_genres=mnozenie_przez_stala(genres,stala_genres)
    sumowanie=(fragment_A_B(przemnozony_wektor_genres,subgenres))
    #print(df['subgenres'])
    suma_laczna_normaliz_genres_themes=sumowanie[0]+sumowanie[1]
    return normalizacja(przemnozony_wektor_genres,suma_laczna_normaliz_genres_themes),normalizacja(subgenres,suma_subgenres)

df['normal_genres']=df.apply(lambda row: (czesc_AB(row['genres'],row['subgenres']))[0],axis=1)
df['normal_subgenres']=df.apply(lambda row: (czesc_AB(row['genres'],row['subgenres']))[1],axis=1)
#print(df)
########################################################## DOTAD WYLICZA ZNORMALIZOWANE WEKTORY
#genres | subgenres | normal_genres | normal_subgenres
#
#


#tworzy laczony wektor wszystkich istotnych zmiennych (poza subgenres)
def merge_vectors(multiplayer_x,mood_x,recently_released_x,mainstream_x,normal_genres_x):
    x=[multiplayer_x,mood_x,recently_released_x,mainstream_x]
    lista=list(x)+(normal_genres_x)
    print(x)
    return lista
df['laczny_wektor']=df.apply(lambda row: (merge_vectors(row['multiplayer'],row['mood'],row['recently_released'],row['mainstream'],row['normal_genres'])),axis=1)
df2=df[['id','laczny_wektor']]
#df['laczny_w']=list(merge(str(df['mood']),str(df['multiplayer'])))
print(df2)

#generuje liste kombinacji
from itertools import combinations
kombinacje=list(combinations(df2.id, 2))
#print(kombinacje)
kombinacje=pd.DataFrame(kombinacje)
kombinacje.rename(columns={0:'id'},inplace=True)
kombinacje=pd.merge(kombinacje,df2, on='id')
kombinacje.rename(columns={'id':'id1', 'laczny_wektor':'laczny_wektor1'},inplace=True)
kombinacje.rename(columns={1:'id'},inplace=True)
kombinacje=pd.merge(kombinacje,df2, on='id')
kombinacje.rename(columns={'id':'id2', 'laczny_wektor':'laczny_wektor2'},inplace=True)
print(kombinacje)


#liczy odleglosc taksówkową/miejską miedzy wektorami/punktami
def odleglosc(x,y):
 # print(x)
 # print(y)
  return sum(abs(a-b) for a,b in zip(x,y))

kombinacje['distance']=kombinacje.apply(lambda row: (odleglosc(row['laczny_wektor1'],row['laczny_wektor2'])),axis=1)






#ostatecznie zostają nam 2 dataframe'y
#kombinacje wyglada nastepujaco: (zawiera on informacje dot bliskości tytułów wzg siebie)
#  id1 |  wektor_laczny1 | id2  |  wektor_laczny2  | odleglosc

#df wyglada nastepujaco:  (główny dataframe z informacjami odnośnie gier do scoringu)
# id | genres | mainstream | mood | multiplayer | recently_released | subgenres | tytul | normal_genres | normal_subgenres | laczny_wektor
print(kombinacje)
print(df)

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
######                      ODTAD CZESC Z UZYTKOWNIKIEM
####################################################################################################
####################################################################################################
####################################################################################################


#importujemy df i kombinacje  (do zamiany w import csv)
df=df
kombinacje=kombinacje



#####################################
#dane wprowadzone przez uzytkownika, po wyklikaniu formularza; w finalnej wersji będą zastąpione przez dane od użytkownika
vector_genres_uzytk=[0,1,0,0,1,1,0,0,0,1,1,1,0,0,1,1,1,0,0,0,0]  #pierwsze 7 wartosci - genres, pozostale - themes
vector_subgenres_uzytk=[0,0,1,1,0,0,0,0,0,1,0,0,0,0,1,1,1,0,0,1,0,1,1,1,0,1]
multiplayer_uzytk=2
mainstream_uzytk=2
recently_released_uzytk=0  #0 - older, 1- newer , 2- surprise
mood_uzytk=2
id_favorite_uzytk=4  # id ulubionej gry użytkownika, podanej w formularzu; na chwilę obecną zakładam, że użytkownik poda co najwyżej 1 tytuł

####################################3



#Filtruje po multiplayer
df1=df[['id','tytul','normal_genres','normal_subgenres','multiplayer','mainstream','recently_released','mood']]
if multiplayer_uzytk==0:
    df1=df1[(df1['multiplayer']==0) | (df1['multiplayer']==2)]
if multiplayer_uzytk==1:
    df1=df1[(df1['multiplayer']==1) | (df1['multiplayer']==2)]



#LICZY W1 i W2
def iloczyn_skalarny(wektor_gry,wektor_uzytk):
    return np.dot(wektor_gry,wektor_uzytk)

df1['W1']=(df1.apply(lambda row: (iloczyn_skalarny(row['normal_genres'],vector_genres_uzytk)),axis=1))
df1['W2']=(df1.apply(lambda row: (iloczyn_skalarny(row['normal_subgenres'],vector_subgenres_uzytk)),axis=1))


#W3 - MOOD

def nastroj(nastroj_gry,nastroj_uzytk):
    wynik_w3=0
    if nastroj_uzytk==0:
        if nastroj_gry == 0:
            wynik_w3 = 1
        elif nastroj_gry == 1:
            wynik_w3 = 0
        else:
            wynik_w3 = 0.5
    elif nastroj_uzytk == 1:
        if nastroj_gry == 0:
            wynik_w3 = 0
        elif nastroj_gry == 1:
            wynik_w3 = 1
        else:
            wynik_w3 = 0.5
    elif nastroj_uzytk == 2:
        wynik_w3 = 1
    return wynik_w3

df1['W3']=df1.apply(lambda row: (nastroj(row['mood'],mood_uzytk)),axis=1)


#liczy W4 -nowosc gry
def starosc(czas_od_premiery,nowoscuzytk):
    #wynik_w4=0
    if nowoscuzytk==0: #(uzytkownik wybral gry starsze)
        wynik_w4 = (1 / (1 + math.exp(-(czas_od_premiery - 3) * 1.5)))
        #print(wynik_w4)
    elif nowoscuzytk==1: #(uzytkownik wybral gry nowsze)
        wynik_w4 = (1-(1 / (1 + math.exp(-(czas_od_premiery - 3.5) * 1.5))))
    else:              #opcja surprise
        wynik_w4=1
    return wynik_w4


df1['W4']=df1.apply(lambda row: (starosc(row['recently_released'],recently_released_uzytk)),axis=1)



#liczy W5 - mainstream
def mainstreamer(gra,uzytkow):
    mainstream_gry=0
    if uzytkow ==0:
        if gra==0:
            mainstream_gry = 1
        elif gra==1:
            mainstream_gry = 0
        else:
            mainstream_gry = 0.35
    elif uzytkow == 1:
        if gra == 0:
            mainstream_gry = 0
        elif gra == 1:
            mainstream_gry = 1
        else:
            mainstream_gry = 0.45
    elif uzytkow == 2:
        mainstream_gry = 1
    return mainstream_gry

df1['W5']=df1.apply(lambda row: (mainstreamer(row['mainstream'],mainstream_uzytk)),axis=1)


#w6 - ulubiona gra
df1['W6']=0



#w7 - bliskosc ulubionej gry
#zakladam ze po kliknieciu jakies gry z listy,otrzymamy id tej gry, jesli zaś uzyrkownik nie wpisał nic, to dostaniemy null
from numbers import Number
def liczy_wartosc_exp(zmienna_z):
    z=float(zmienna_z)
    z=math.exp(-z * 0.1)
    return(z)
    #if
def bliskosc(wartosc):
    if wartosc == None:
        #print('nic nie bylo w ulubionych grach')
        return 0
    else:
#        wycinek_kombinacji=kombinacje[(kombinacje['id1']==wartosc) | (kombinacje['id2']==wartosc)]
        wycinek_kombinacji = kombinacje[(kombinacje.loc[:, ('id1')] == wartosc) | (kombinacje.loc[:, ('id2')] == wartosc)]

        #print(wycinek_kombinacji)
        cond = (wycinek_kombinacji.id2 == wartosc)
        wycinek_kombinacji.loc[cond, ['id2', 'id1','laczny_wektor2','laczny_wektor1']] = wycinek_kombinacji.loc[cond, ['id1', 'id2','laczny_wektor1','laczny_wektor2']].values
        #wycinek_kombinacji['distance']=float(wycinek_kombinacji['distance'])
        wycinek_kombinacji['W7'] = wycinek_kombinacji.apply(lambda row: (liczy_wartosc_exp(row['distance'])), axis=1)
        #wycinek_kombinacji1=wycinek_kombinacji[['id1','W7']]
        wycinek_kombinacji1 = wycinek_kombinacji.loc[:, ('id2', 'W7')]
        wycinek_kombinacji1.rename(columns={'id2': 'id'}, inplace=True)
        return wycinek_kombinacji1

przemielone_kombinacje=bliskosc(id_favorite_uzytk)
df1=pd.merge(df1,przemielone_kombinacje,on='id')
#DOPISAC ZABEZPIECZENIA, gdy nie kliknal niczego

def count_score(z1,z2,z3,z4,z5,z6,z7):
    score=1*z1+1*z2+1*z3+1*z4+1*z5+1*z6+1*z7
    return score


df1['scoring']=df1.apply(lambda row: (count_score(row['W1'],row['W2'],row['W3'],row['W4'],row['W5'],row['W6'],row['W7'])),axis=1)

#print(df)



print(df1)