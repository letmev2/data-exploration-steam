import pandas as pd

import itertools
from ast import literal_eval
df1=pd.read_csv('D:/dane/letme/core_gamequestionnairedata.csv',
                sep=",")#,index_col='id')

df1=df1.sort_values(by=['id'])
#print(df1.head())
df1=df1[df1['is_valid']=='t']
#df1=df1.sort_index('id')

def kolejnosclisty(list1):
    list1=literal_eval(list1)  #konwertuje string np [1,2] na liste
    list1.sort(reverse=False)
    return list1

#def czywystepujewliscie(listta1,elementzlisty):
    #    if elementzlisty is in listta1:
  #          return 1



df1['themes']=df1.apply(
        lambda row: (kolejnosclisty(row['themes'])),axis=1)

print(df1.head(28))
dfmood0=df1[df1['mood']==0]
dfmood1=df1[df1['mood']==1]
#df4=df1[df1['mood'].isnull()]
#df0=pd.DataFrame()
print('----------------------------------------------')
print('Games counted:                      '+str(df1['id'].count()))
print('How many games have mood=0:          '+str(dfmood0['id'].count()))
print('How many games have mood=1:          '+str(dfmood1['id'].count()))
print('----------------------------------------------')
dfmultiplayer0=df1[df1['multiplayer']==0]
dfmultiplayer1=df1[df1['multiplayer']==1]
print('How many games have multiplayer=0:   '+str(dfmultiplayer0['id'].count()))
print('How many games have multiplayer=1:   '+str(dfmultiplayer1['id'].count()))
print('----------------------------------------------')
dfmainstream0=df1[df1['mainstream']==0]
dfmainstream1=df1[df1['mainstream']==1]
print('How many games have mainstream=0:   '+str(dfmainstream0['id'].count()))
print('How many games have mainstream=1:   '+str(dfmainstream1['id'].count()))
print('----------------------------------------------')
dfrecently_released0=df1[df1['recently_released']==0]
dfrecently_released1=df1[df1['recently_released']==1]
print('How many games have recently_released=0:   '+str(dfrecently_released0['id'].count()))
print('How many games have recently_released=1:   '+str(dfrecently_released1['id'].count()))
print('----------------------------------------------')
#print(df1[df1.genres.apply(lambda x: 'indie' in x)])       #wyswietla te elementy ze slowo indie znajduje sie w liscie w kolumnie
#print('----------------------------------------------')
dftheme0=df1[df1.themes.apply(lambda x: 0 in x)]
print('How many games have theme 0:   '+str(dftheme0['id'].count()))
dftheme1=df1[df1.themes.apply(lambda x: 1 in x)]
print('How many games have theme 1:   '+str(dftheme1['id'].count()))
dftheme2=df1[df1.themes.apply(lambda x: 2 in x)]
print('How many games have theme 2:   '+str(dftheme2['id'].count()))
dftheme3=df1[df1.themes.apply(lambda x: 3 in x)]
print('How many games have theme 3:   '+str(dftheme3['id'].count()))
dftheme4=df1[df1.themes.apply(lambda x: 4 in x)]
print('How many games have theme 4:   '+str(dftheme4['id'].count()))
dftheme5=df1[df1.themes.apply(lambda x: 5 in x)]
print('How many games have theme 5:   '+str(dftheme5['id'].count()))
dftheme6=df1[df1.themes.apply(lambda x: 6 in x)]
print('How many games have theme 6:   '+str(dftheme6['id'].count()))
dftheme7=df1[df1.themes.apply(lambda x: 7 in x)]
print('How many games have theme 7:   '+str(dftheme7['id'].count()))
dftheme8=df1[df1.themes.apply(lambda x: 8 in x)]
print('How many games have theme 8:   '+str(dftheme8['id'].count()))
dftheme9=df1[df1.themes.apply(lambda x: 9 in x)]
print('How many games have theme 9:   '+str(dftheme9['id'].count()))
dftheme10=df1[df1.themes.apply(lambda x: 10 in x)]
print('How many games have theme 10:   '+str(dftheme10['id'].count()))
dftheme11=df1[df1.themes.apply(lambda x: 11 in x)]
print('How many games have theme 11:   '+str(dftheme11['id'].count()))
dftheme12=df1[df1.themes.apply(lambda x: 12 in x)]
print('How many games have theme 12:   '+str(dftheme12['id'].count()))
dftheme13=df1[df1.themes.apply(lambda x: 13 in x)]
print('How many games have theme 13:   '+str(dftheme13['id'].count()))
print('----------------------------------------------')
#############################################   lista wszystkich wystepujacych gatunkow
lista_gatunkow=[]
def lista_genres(lista2):
    lista2=literal_eval(lista2)
    for i in lista2:
        if i not in lista_gatunkow:
            lista_gatunkow.append(i)

df1.apply(lambda row: (lista_genres(row['genres'])),axis=1)
lista_gatunkow_podstawowa=lista_gatunkow
print('Lista gatunków genres:')
print(lista_gatunkow_podstawowa)
lista_gatunkow=[]
df1.apply(lambda row: (lista_genres(row['more_genres'])),axis=1)
print('Lista rozszerzona tagów more_genres:')
print(lista_gatunkow)
print('----------------------------------------------')
##############################################                            SPOSOB NA TWORZENIE DATAFRAMOW W PETLI
dfgenres = {name: pd.DataFrame() for name in lista_gatunkow_podstawowa}
for name, df in dfgenres.items():
    dfgenres[str(name)] = df1[df1.genres.apply(lambda x: name in x)]
    print('How many games have genre '+str(name)+':   ' + str(dfgenres[str(name)]['id'].count()))
  #  print(df.head())
  #  print(name)
#print(df.head(8))

#print(dfgenres['indie'])
#for name, df in dfgenres.items():
for nazwa,frejm in itertools.combinations(dfgenres.items(), 2):
    print(list(nazwa))
   # for name1, df1 in dfgenres:
  #      print(str(name)+str(name1))
 #   del dfgenres[df]
  #  print(df.head())

#def pairwise(input):
 #   for nazwa in input.values():
  #      for pair in itertools.combinations(nazwa.items(), 2):
   #         yield dict(pair)
       # for i in dict(pair):
      #      print(i)

#generator = pairwise(dfgenres)
#print(generator)
#for i,v in generator:
 #   print(i,v)

#print((pairwise(dfgenres)))
#print(dfgenresaction)
##############################################
#for name in dfgenres.items():
 #   print(name)







#dft14=df1[df1.themes.apply(lambda x: 14 in x)]
#print('How many people chose theme 14:   '+str(dft14.size))
#for i in range(15):
   # theta='dft'+str(i)
  #  theta=pd.DataFrame(theta)
 #   theta=df1[df1.themes.apply(lambda x: 1 in x)]
#print(dft1)
#df0['People counted:']=int(df1.size)

#print(df0)
#print('Ilosc gier dla mood=NULL:    '+str(df4.size))