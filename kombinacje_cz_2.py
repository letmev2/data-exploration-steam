import pandas as pd
from itertools import combinations
#PIERW UZYC CZESCI 1 PROGRAMU tzn: 'kombinacje_cz_1.py'
ilosc_par=2      #ustawia czy ma szukac kombinacji gier parami, trójkami, itd.

#w przypadku innego pliku z danymi niz ten z kaggle(lub jego modyfikacji/poszerzeniu o nowe gry),
# tabela/plik klucz_gier.csv generowana w czesci 1, musi zostac wygenerowana na nowo
df1=pd.read_csv('D:/projekty/projektkraken/data-exploration-steam/klucz_gier.csv',
                sep=",")#,index_col='id_gry')

df2=pd.read_csv('D:/projekty/projektkraken/data-exploration-steam/purchased.csv',
                sep=",")


################## zakodowuje nazwy gier pod postacia liczbowego id

df2= df2.drop('indeks.1', 1)
df2.sort_values(by=['uzytkow','nazwa'], inplace=True) #sprawi ze nie musimy sie martwic o permutacje typu (gra1,gra2) i (gra2,gra1), bo pierwsza wartosc bedzie liczba mniejsza
#print(df1.head())
#print(df2.head())

df3=pd.merge(df2,df1, on='nazwa')
#df1=df1[['nazwa']]
df3=df3.drop('Unnamed: 0',1)
df3.sort_values(by=['uzytkow','nazwa'], inplace=True)
print(df3.head())


#########################



kombinacje=df3.groupby('uzytkow').apply(lambda x: list(combinations(x['id_gry'], ilosc_par)))
kombinacje=kombinacje.reset_index(name='combo')         #dostajemy tabele wszystkich wystepujacych kombinacji (kombinowanych parami)

#kombinacje=kombinacje[kombinacje['uzytkow']<8000000]  #warunek zbedny, wykorzystany do testowania programu

wynik=pd.DataFrame()
#######################

n=kombinacje['uzytkow'].max()
print(n)
iteracje=120                    #zeby nie zawalilo pamieci, musimy rozlozyc proces na czesci, dla ilosc_par=2, dobrze sprawdzalo sie iteracje=120
for i in range(iteracje):
    kombinacjepetla=kombinacje[(kombinacje['uzytkow']>(i*n/iteracje)) & (kombinacje['uzytkow']<=(i+1)*(n/iteracje))]         #warunek do usuniecia
    print(kombinacjepetla)
    zeta=kombinacjepetla.set_index(['uzytkow'])['combo'].apply(pd.Series).stack().reset_index()#['uzytkow'], name='combo')
    zeta=zeta.drop('level_1', 1)
    wynik=wynik.append(zeta)
    print(zeta)
    print(i)
    print(wynik.shape[0])

wynik.rename(columns={'0':'kombinacje'},inplace=True)
print('Wynik')
print(wynik)
wynik.to_csv('kombinacje'+str(ilosc_par)+'.csv')